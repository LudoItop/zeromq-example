var ipZeroMQ = "54.76.143.172";

var zmq = require('zmq')
    , sockPush = zmq.socket('push')
    , kue = require('kue')
    , jobs = kue.createQueue({
        prefix: 'qa',
        redis: {
            //host: '54.76.143.172',
            host: '192.168.1.116',
            port: 6379
        }
    })
    , sockPull = zmq.socket('pull')
    , jobFinder = kue.Job;


var os = require('os')

var interfaces = os.networkInterfaces();
var addresses = [];
for (k in interfaces) {
    for (k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family == 'IPv4' && !address.internal) {
            addresses.push(address.address)
        }
    }
}


// Populo el kue
for(var i=0;i<=5;i++)
{


    var name = addresses[0] + ":" + i;
    console.log(name);
    var job = jobs.create('MyJob', {
        name: name
    }).priority('normal').save();


// Creo los jobs
    job.on('complete', function(result){
        console.log("Job completed with data ", result);
    }).on('failed', function(){
        console.log("Job failed");
    }).on('progress', function(progress){
        process.stdout.write('\r  job #' + job.id + ' ' + progress + '% complete');
    });

}

var arrayJobs = [];

jobs.process('MyJob', 1, function (job, done){
    /* carry out all the job function here */

    arrayJobs[job.id] = done;
    console.log("Voy a hacer push con ",job.data)
    sockPush.send(JSON.stringify({id: job.id, name: job.data.name}));
    //done();
});

sockPull.connect('tcp://' + ipZeroMQ +':3001');
sockPull.on('message', function(msg){
    console.log("Returned msg " + msg)

    var json = JSON.parse(msg);

    for(var i=0;i<=jobs.workers.length-1;i++)
    {
        if(jobs.workers[i].job.id == json.id)
        {
            arrayJobs[json.id]();
            console.log("Completo ")
            //jobs.workers[i].job.complete();

        }

    }


});

sockPush.bindSync('tcp://' + ipZeroMQ +':3000');
console.log('Server sending jobs to port 3000');
console.log('Server receiving jobs from port 3001');

/*setInterval(function(){
 console.log('sending work');
 sock.send('some work');
 }, 500);*/
var ipZeroMQ = "54.76.143.172";

var zmq = require('zmq')
    , sockPull = zmq.socket('pull')
    , sockPush = zmq.socket('push');

var os = require('os')


// Parte de la recepción
sockPull.connect('tcp://' + ipZeroMQ + ':3000');
sockPush.bindSync('tcp://' + ipZeroMQ + ':3001');

console.log('Client pulling from port 3000');
console.log('Client pushing to port 3001');

sockPull.on('message', function(msg){
    console.log("I have msg " + msg)
    setTimeout(function()
    {
        sockPush.send(msg);
    },1000)
});


